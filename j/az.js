//console.log(JSONdata);
oTemplateData = {};

function truncateStr(str, len){
	if(str.length > len) {
		str = str.substring(0,len-1)+"...";
	}
	
	return str;
}

function renderPage(sort){
	//pass in an optional sort value. 
	//alert(sort);
	
	//clear everything on the page first.
	$('body').empty();

	oTemplateData.title = "Azuqua Marketplace";
	//Render title
	var sTemplate = document.getElementById('az_container').innerHTML;
	var azTitleHTML = $(Mustache.to_html(sTemplate, oTemplateData));
	$('body').append(azTitleHTML);
	
	//Render nav
	var sTemplate = document.getElementById('az_nav').innerHTML;
	var azNavHTML = $(Mustache.to_html(sTemplate, oTemplateData));
	$('#container').append(azNavHTML);
	
	//Bind the nav click  functions.
	$( ".sort" ).bind( "click", function() {
		//alert( "User wants to sort by " + this.id );
		renderPage(this.id);
	});
	
	//log all the rendered HTML for debugging. 
	//console.log($('body').html());
	
	renderFlos();
}

function renderFlos(){
	
	//loop through and render all the flos 
	//JSONdata is being set in the data.js file
	
	//TODO: sort the data if sort exists.
	$.each(JSONdata, function(i, item) {
		//console.log(item.id);
		oTemplateData = {};
		
		oTemplateData.id = item.id;
		oTemplateData.name = item.name;
		
		//truncate long descriptions. Show them in the more info part.
		item.desc = truncateStr(item.desc, 100);
		
		oTemplateData.desc = item.desc;
		oTemplateData.rating = item.rating;
		oTemplateData.author = item.author;
		oTemplateData.date = item.date;
		oTemplateData.price = item.price;
		oTemplateData.max = 5;
		
		//render each flo card template.
		var sTemplate = document.getElementById('az_flo_card').innerHTML;
		var azFloCardHTML = $(Mustache.to_html(sTemplate, oTemplateData));
		$('#container').append(azFloCardHTML);
		
	});
}

$( document ).ready(function() {
    renderPage();
});
